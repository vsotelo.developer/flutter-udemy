import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IconHeader extends StatelessWidget {
  const IconHeader({
    required this.title,
    required this.subTitulo,
    required this.icon,      
    this.iconSize = 80,
    this.color1 = Colors.blue,
    this.color2 = Colors.blueGrey,    
  });

  final String title;
  final String subTitulo;
  final IconData icon;    
  final double iconSize;
  final Color color1;
  final Color color2;  

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _HeadGradiantBack(color1, color2),
        Positioned(
          top: -30,
          left: -50,
          child: FaIcon(FontAwesomeIcons.plus, size: 250, color: Colors.white.withOpacity(0.2))
        ),
        Column(
          children: <Widget>[
            const SizedBox(height: 80, width: double.infinity),
            Text(title, style: TextStyle(fontSize: 20, color: Colors.white.withOpacity(0.7))),
            Text(subTitulo, style: TextStyle(fontSize: 25, color: Colors.white.withOpacity(0.7), fontWeight: FontWeight.bold)),
            const SizedBox(height: 20),
            FaIcon(icon, size: iconSize, color: Colors.white)
          ],
        )
      ],
    );
  }
}

class _HeadGradiantBack extends StatelessWidget {
  const _HeadGradiantBack(this.color1, this.color2);

  final Color color1;
  final Color color2;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,
      decoration:  BoxDecoration(          
          borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(80)),
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: <Color>[color1, color2],
          )),
    );
  }
}
