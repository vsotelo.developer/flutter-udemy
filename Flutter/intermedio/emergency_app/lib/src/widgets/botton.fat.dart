import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ButtonFat extends StatelessWidget {
  const ButtonFat({
    Key? key,
    required this.text,
    required this.icon,
    required this.onPress,
    this.color1 = Colors.blue,
    this.color2 = Colors.blueGrey,
  }) : super(key: key);

  final String text;
  final IconData icon;
  final VoidCallback onPress;
  final Color color1;
  final Color color2;  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Stack(
        children: <Widget>[
           _ButtonFatBackground(icon, color1, color2),
           Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
               const SizedBox(height: 140, width: 40),
               FaIcon(icon, size: 40, color: Colors.white),
               const SizedBox(width: 20),
               Expanded(child: Text(text, style: const TextStyle(color: Colors.white, fontSize: 18))),
               const FaIcon(FontAwesomeIcons.chevronRight, color: Colors.white),
               const SizedBox(width: 40),
             ], 
           )
        ]
      ),
    );
  }
}

class _ButtonFatBackground extends StatelessWidget {
  const _ButtonFatBackground (this.icon, this.color1, this.color2);

  final IconData icon;
  final Color color1;
  final Color color2;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect( // ! Corta todo lo que se sale de mi widget child, generalmente le mando el mismo borde del widget interno para poder cortar desde ahi
        borderRadius:  BorderRadius.circular(15),
        child: Stack(
          children: <Widget>[
            Positioned( //! Mueve el widget child en cualquier parte dentro de mi Stack
              right: -20,
              top: -20,
              child: FaIcon(icon, size: 150, color: Colors.white.withOpacity(0.2))
            ),
          ],
        ),
      ),
      margin: const EdgeInsets.all(20),
      width: double.infinity,
      height: 100,        
      decoration: BoxDecoration(
        color: Colors.red,
        boxShadow: <BoxShadow>[
          BoxShadow (color: Colors.black.withOpacity(0.2), offset: const Offset(4, 6), blurRadius: 10)
        ],
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
          colors: <Color>[ color1, color2 ]
        ),
      ),
    );
  }
}