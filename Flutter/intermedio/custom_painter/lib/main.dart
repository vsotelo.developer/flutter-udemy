import 'package:custom_painter/src/pages/subs/animations.page.dart';
import 'package:custom_painter/src/pages/subs/headers.page.dart';
import 'package:custom_painter/src/pages/home.screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UiProvider()),      
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Diseños App',
        initialRoute: 'home',
        routes: {
          'home':(context) => const HomeScreen(),
          'headers':(context) => const HeadersPage(),
          'animations':(context) => const AnimationsPage(),
        },
      ),
    );
  }
}
