import 'package:flutter/material.dart';

import '../../widgets/headers.dart';

class HeadersPage extends StatelessWidget {
  const HeadersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //body: HeaderDiagonal(),
      body: PageView(
        scrollDirection: Axis.vertical,
        children: const [
          HeaderCuadrado(),
          HeaderBordesRedondeados(),
          HeaderDiagonal(),
          HeaderTriangular(),
          HeaderPico(),
          HeaderCurvo(),
          HeaderWave(),
          HeaderWaveGradiant(),          
        ],
      ),
    );
  }
}
