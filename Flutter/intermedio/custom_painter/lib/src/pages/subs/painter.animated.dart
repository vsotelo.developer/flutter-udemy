import 'package:flutter/material.dart';

import '../../widgets/paint_animated/circular.progres.temp.dart';

class PainterAnimatedPage extends StatefulWidget {
  const PainterAnimatedPage({Key? key}) : super(key: key);

  @override
  State<PainterAnimatedPage> createState() => _PainterAnimatedPageState();
}

class _PainterAnimatedPageState extends State<PainterAnimatedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: const [
          GraficasCircularesPage(),
        ],
      ),
    );
  }
}
