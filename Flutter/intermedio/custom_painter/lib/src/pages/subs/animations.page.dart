import 'package:custom_painter/src/widgets/animations/cuadrado_animado_page.dart';
import 'package:flutter/material.dart';

import '../../widgets/animations/animation.curve.dart';
import '../../widgets/animations/animation.move.dart';
import '../../widgets/animations/animation.opacity.dart';
import '../../widgets/animations/animation.scale.dart';
import '../../widgets/animations/animations.cuadrado.dart';
import '../../widgets/animations/animations.status.dart';

class AnimationsPage extends StatelessWidget {
  const AnimationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //body: HeaderDiagonal(),
      body: PageView(
        scrollDirection: Axis.vertical,
        children: const [
          AnimationsCuadrado(),
          AnimationsCuadradoCurvo(),
          AnimationsCuadradoOpacity(),
          AnimationsCuadradoMove(),
          AnimationsCuadradoScale(),
          AnimationsStatus(),
          CuadradoAnimadoPage(),
        ],
      ),
    );
  }
}
