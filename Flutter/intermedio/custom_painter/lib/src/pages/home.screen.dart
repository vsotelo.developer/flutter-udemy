import 'package:custom_painter/src/pages/subs/animations.page.dart';
import 'package:custom_painter/src/pages/subs/headers.page.dart';
import 'package:custom_painter/src/pages/subs/painter.animated.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Diseños App'),
      ),
      body: _HomePageBody(),
      bottomNavigationBar: _CustomNavigationBar(),
    );
  }
}

class _HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Obtener el selected menu opt
    final uiProvider = Provider.of<UiProvider>(context);

    // Cambiar para mostrar la pagina respectiva
    final currentIndex = uiProvider.selectedMenuOpt;

    switch (currentIndex) {
      case 0:        
        return const HeadersPage();
      case 1:        
        return const AnimationsPage();
      case 2:        
        return const PainterAnimatedPage();        
      default:        
        return const HeadersPage();
    }
  }
}


class _CustomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Obtener el selected menu opt
    final uiProvider = Provider.of<UiProvider>(context);

    return BottomNavigationBar(
      onTap: (int i) => uiProvider.selectedMenuOpt = i,
      currentIndex: uiProvider.selectedMenuOpt,
      elevation: 0,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(Icons.add_to_queue_sharp ), label: 'Headers'),
        BottomNavigationBarItem(icon: Icon(Icons.animation), label: 'Animations'),
        BottomNavigationBarItem(icon: Icon(Icons.format_paint_sharp), label: 'Paint Animate'),
      ],
    );
  }
}

class UiProvider extends ChangeNotifier {
  int _selectedMenuOpt = 0; // default

  int get selectedMenuOpt {
    return _selectedMenuOpt;
  }

  set selectedMenuOpt(int i) {
    _selectedMenuOpt = i;
    notifyListeners();
  }
}
