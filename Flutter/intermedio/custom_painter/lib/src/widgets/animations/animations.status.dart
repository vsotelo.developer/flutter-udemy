import 'package:flutter/material.dart';

import 'dart:math' as Math;

class AnimationsStatus extends StatelessWidget {
  const AnimationsStatus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: _CuadradoAnimado()),
    );
  }
}

class _CuadradoAnimado extends StatefulWidget {
  const _CuadradoAnimado({
    Key? key,
  }) : super(key: key);

  @override
  State<_CuadradoAnimado> createState() => _CuadradoAnimadoState();
}

class _CuadradoAnimadoState extends State<_CuadradoAnimado>
    with SingleTickerProviderStateMixin {
  AnimationController? animationController;
  Animation<double>? rotation;
  
  Animation<double>? opacity;
  Animation<double>? opacityOut;

  Animation<double>? moverDerecha;
  Animation<double>? agrandar;

  @override
  void initState() {
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 4000));

    rotation = Tween(begin: 0.0, end: 2 * Math.pi).animate(
      CurvedAnimation( parent: animationController!, curve: Curves.elasticOut),
    );

    // ! sin controlar el tiempo
    //opacity = Tween(begin: 0.1, end: 1.0).animate(animationController!);

    opacity = Tween(begin: 0.1, end: 1.0).animate(
       CurvedAnimation( parent: animationController!, curve: const Interval(0, 0.25, curve: Curves.easeOut)), // 0, 0.25 en vace a porcentaje
    );

    opacityOut = Tween(begin: 0.0, end: 1.0).animate(
       CurvedAnimation( parent: animationController!, curve: const Interval(0.75, 1.0, curve: Curves.easeOut)), // 0, 0.25 en vace a porcentaje
    );

    moverDerecha = Tween(begin: 0.0, end: 200.0).animate(
        CurvedAnimation( parent: animationController!, curve: Curves.easeOut),  
    );

    agrandar = Tween(begin: 0.0, end: 2.0).animate(// desde 0 es minuscyulo y el end es propocional a su tamaño original, osea 1 es el original y 2 seria el doble...
        CurvedAnimation( parent: animationController!, curve: Curves.easeOut),  
    );

    animationController!.addListener(() {
      //print('Status: ${animationController!.status}');
      if (animationController!.status == AnimationStatus.completed) {
        animationController!.repeat();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    animationController!.forward();

    return AnimatedBuilder(
      animation: animationController!,
      child: _Rectangulo(),
      builder: (BuildContext context, Widget? childRectangulo) {
        
        print('Status: ${animationController!.status}');

        return Transform.translate(
          offset: Offset(moverDerecha!.value, 0),
          child: Transform.rotate(
            angle: rotation!.value,
            child: Opacity(
              opacity: opacity!.value - opacityOut!.value,
              child: Transform.scale(scale: agrandar!.value, child: childRectangulo),
            )
          ),
        );
      },
    );
  }
}

class _Rectangulo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      decoration: const BoxDecoration(color: Colors.blue),
    );
  }
}
