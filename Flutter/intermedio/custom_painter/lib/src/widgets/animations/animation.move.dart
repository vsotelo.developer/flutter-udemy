import 'package:flutter/material.dart';

import 'dart:math' as Math;

class AnimationsCuadradoMove extends StatelessWidget {
  const AnimationsCuadradoMove({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: _CuadradoAnimado()),
    );
  }
}

class _CuadradoAnimado extends StatefulWidget {
  const _CuadradoAnimado({
    Key? key,
  }) : super(key: key);

  @override
  State<_CuadradoAnimado> createState() => _CuadradoAnimadoState();
}

class _CuadradoAnimadoState extends State<_CuadradoAnimado>
    with SingleTickerProviderStateMixin {
  AnimationController? animationController;
  Animation<double>? rotation;
  Animation<double>? opacity;
  Animation<double>? moverDerecha;

  @override
  void initState() {
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 4000));

    rotation = Tween(begin: 0.0, end: 2 * Math.pi).animate(
      CurvedAnimation( parent: animationController!, curve: Curves.elasticOut),
    );

    // ! sin controlar el tiempo
    //opacity = Tween(begin: 0.1, end: 1.0).animate(animationController!);

    opacity = Tween(begin: 0.1, end: 1.0).animate(
       CurvedAnimation( parent: animationController!, curve: const Interval(0, 0.25, curve: Curves.easeOut)), // 0, 0.25 en vace a porcentaje
    );

    moverDerecha = Tween(begin: 0.0, end: 200.0).animate(
        CurvedAnimation( parent: animationController!, curve: Curves.easeOut),  
    );

    animationController!.addListener(() {
      if (animationController!.status == AnimationStatus.completed) {
        animationController!.reset();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    animationController!.forward();

    return AnimatedBuilder(
      animation: animationController!,
      child: _Rectangulo(),
      builder: (BuildContext context, Widget? child) {
        return Transform.translate(
          offset: Offset(moverDerecha!.value, 0),
          child: Transform.rotate(
            angle: rotation!.value,
            child: Opacity(
              opacity: opacity!.value,
              child: child,
            )
          ),
        );
      },
    );
  }
}

class _Rectangulo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      decoration: const BoxDecoration(color: Colors.blue),
    );
  }
}
