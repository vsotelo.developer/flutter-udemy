import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
//! No se invoca en niguna parte, lo hice para practicar al inicio
class CircularProgressPage extends StatefulWidget {
  const CircularProgressPage({Key? key}) : super(key: key);

  @override
  State<CircularProgressPage> createState() => _CircularProgressPageState();
}

class _CircularProgressPageState extends State<CircularProgressPage> with SingleTickerProviderStateMixin{
  AnimationController? controller;
  double porcentaje = 0;
  double nuevoPorcentaje = 0.0;

  @override
  void initState() {
    controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 800));
    controller!.addListener(() {
       //print('Valor del controller: ${controller!.value}');
       setState(() {
          porcentaje = lerpDouble(porcentaje, nuevoPorcentaje, controller!.value)!;  
       });
    });
    super.initState();
  }

  @override
  void dispose() {    
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.refresh),
        backgroundColor: Colors.pink,
        //elevation: ,
        onPressed: () {
          porcentaje = nuevoPorcentaje;
          nuevoPorcentaje += 10;
          if (nuevoPorcentaje > 100) {
           nuevoPorcentaje = 0;
           porcentaje = 0;
          }
          controller!.forward(from: 0.0);

          setState(() {});
        },
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(5),
          width: 300,
          height: 300,
          //color: Colors.red,
          child: CustomPaint(
            painter: _MiRadialProgressPainter(porcentaje: porcentaje),
          ),
        ),
      ),
    );
  }
}

class _MiRadialProgressPainter extends CustomPainter {
  _MiRadialProgressPainter({required this.porcentaje});

  final porcentaje;

  @override
  void paint(Canvas canvas, Size size) {
    // *Inicio de Circulo completado
    final paintCirculo = Paint();
    paintCirculo.strokeWidth = 4;
    paintCirculo.color = Colors.grey;
    paintCirculo.style = PaintingStyle.stroke;
    // ancho = width | altura = height

    final double altura = size.height;
    final double ancho = size.width;

    final Offset center = Offset(ancho * 0.5, altura * 0.5);
    final double radius = min(ancho * 0.5, altura * 0.5);

    canvas.drawCircle(center, radius, paintCirculo);
    // *Fin de Circulo completado

    // *Inicio de Arco completado
    final paintArco = Paint();
    paintArco.strokeWidth = 10;
    paintArco.color = Colors.pink;
    paintArco.style = PaintingStyle.stroke;

    // Parte que se deberá ir llenando -- 2 * pi es una vuelta completa
    double arcAngle = 2 * pi * (porcentaje / 100);

    canvas.drawArc(
        Rect.fromCircle(
            center: center,
            radius: radius), // El espacio deonde queremos ubicar el dibujo
        -pi / 2, // angulo inicial
        arcAngle, // angulo del arco
        false, // unir el arco con el inicio
        paintArco // pincel
        );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
