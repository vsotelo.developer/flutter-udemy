import 'package:flutter/material.dart';

import '../radial.progress.dart';

class GraficasCircularesPage extends StatefulWidget {
  const GraficasCircularesPage({Key? key}) : super(key: key);

  @override
  State<GraficasCircularesPage> createState() => _GraficasCircularesPageState();
}

class _GraficasCircularesPageState extends State<GraficasCircularesPage> {
  double porcentaje = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.refresh),
        onPressed: () {
          setState(() {
            porcentaje += 10;
            if (porcentaje > 100) porcentaje = 0;
          });
        },
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _CustomRadialProgress(porcentaje: porcentaje, progressColor: Colors.red, gradiantRequired: true),
              _CustomRadialProgress(porcentaje: porcentaje, progressColor: Colors.blue)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _CustomRadialProgress(porcentaje: porcentaje, progressColor: Colors.yellow),
              _CustomRadialProgress(porcentaje: porcentaje, progressColor: Colors.brown)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _CustomRadialProgress(porcentaje: porcentaje, progressColor: Colors.pink),
              _CustomRadialProgress(porcentaje: porcentaje, progressColor: Colors.green)
            ],
          )
        ],
      ),
    );
  }
}

class _CustomRadialProgress extends StatelessWidget {
  const _CustomRadialProgress({
    Key? key,
    required this.porcentaje, required this.progressColor, this.gradiantRequired = false
  }) : super(key: key);

  final double porcentaje;
  final Color progressColor;
  final bool gradiantRequired;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 180,
      height: 180,
      child: RadialProgress(
          porcentaje: porcentaje,
          progressColor: progressColor,
          requiredGradiant: gradiantRequired,
          grosorProgress: 10),
      //child: Text('$porcentaje %', style: const TextStyle(fontSize: 50.0))
    );
  }
}
