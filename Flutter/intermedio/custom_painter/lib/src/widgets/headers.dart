import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeaderCuadrado extends StatelessWidget {
  const HeaderCuadrado({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        height: 300,
        color: const Color.fromARGB(255, 0, 0, 0),
      ),
      Container(
          //color: Colors.black45.withOpacity(0.5),
          child: const Center(child: Text('CUADRADO')))
    ]);
  }
}

class HeaderBordesRedondeados extends StatelessWidget {
  const HeaderBordesRedondeados({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        height: 300,
        decoration: const BoxDecoration(
            color: Color.fromARGB(255, 0, 0, 0),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40),
                bottomRight: Radius.circular(40))),
      ),
      Container(
          //color: Colors.black45.withOpacity(0.5),
          child: const Center(child: Text('BORDE REDONDO')))
    ]);
  }
}

class HeaderDiagonal extends StatelessWidget {
  const HeaderDiagonal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: CustomPaint(painter: _HeaderDiagonalPainter()),
        ),
        Container(
            //color: Colors.black45.withOpacity(0.5),
            child: const Center(child: Text('DIAGONAL')))
      ],
    );
  }
}

class HeaderTriangular extends StatelessWidget {
  const HeaderTriangular({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: CustomPaint(painter: _HeaderDiagonalTriangularPainter()),
        ),
        Container(
            //color: Colors.black45.withOpacity(0.5),
            child: const Center(
                child: Text('TRIANGULAR',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white))))
      ],
    );
  }
}

class HeaderPico extends StatelessWidget {
  const HeaderPico({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: CustomPaint(painter: _HeaderPicoPainter()),
        ),
        Container(
            //color: Colors.black45.withOpacity(0.5),
            child: const Center(
                child: Text('PICO',
                    style: TextStyle(fontWeight: FontWeight.bold))))
      ],
    );
  }
}

class HeaderCurvo extends StatelessWidget {
  const HeaderCurvo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: CustomPaint(painter: _HeaderCurvoPainter()),
        ),
        Container(
            //color: Colors.black45.withOpacity(0.5),
            child: const Center(
                child: Text('CURVO',
                    style: TextStyle(fontWeight: FontWeight.bold))))
      ],
    );
  }
}

class HeaderWave extends StatelessWidget {
  const HeaderWave({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: CustomPaint(painter: _HeaderOlaPainter()),
        ),
        Container(
            //color: Colors.black45.withOpacity(0.5),
            child: const Center(
                child:
                    Text('OLA', style: TextStyle(fontWeight: FontWeight.bold))))
      ],
    );
  }
}

class HeaderWaveGradiant extends StatelessWidget {
  const HeaderWaveGradiant({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: CustomPaint(painter: _HeaderOlaPainterGradiant()),
        ),
        Container(
            //color: Colors.black45.withOpacity(0.5),
            child: const Center(
                child: Text('OLA + GRADIANT',
                    style: TextStyle(fontWeight: FontWeight.bold))))
      ],
    );
  }
}

//! DIBUJOS

class _HeaderDiagonalPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint(); // ! Lapiz mongol :v

    // Propiedades
    lapiz.color = const Color.fromARGB(255, 0, 0, 0);
    lapiz.style = PaintingStyle.fill; // ! pinta todo
    //lapiz.style = PaintingStyle.stroke; // ! solo el grueso de la linea
    lapiz.strokeWidth = 2;

    final path = Path();
    // Dibujar con el path y el lapiz

    // lineTo trazar linea
    // moveTo mover lapiz

    path.moveTo(0, size.height * 0.35);
    path.lineTo(size.width, size.height * 0.30);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    path.lineTo(0, size.height * 0.5);

    canvas.drawPath(path, lapiz);
  }

  // !. Revisar en documentacion de Flutter
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class _HeaderDiagonalTriangularPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint(); // ! Lapiz mongol :v

    // Propiedades
    lapiz.color = const Color.fromARGB(255, 0, 0, 0);
    lapiz.style = PaintingStyle.fill; // ! pinta todo
    //lapiz.style = PaintingStyle.stroke; // ! solo el grueso de la linea
    lapiz.strokeWidth = 2;

    final path = Path();
    // Dibujar con el path y el lapiz

    // lineTo trazar linea
    // moveTo mover lapiz

    /*path.moveTo(0, size.height * 0.50);
    path.lineTo(size.width, size.height * 0.50);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    path.lineTo(0, size.height * 0.5);
    ancho = width
    altura = heigth
    */
    path.moveTo(0,
        0); // Esto ya no es necesario, porque siempre inicia en 0,0, ! lo pongo porque quiero
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.lineTo(0,
        0); // Esto ya no es necesario, porque siempre inicia en 0,0, ! lo pongo porque quiero

    canvas.drawPath(path, lapiz);
  }

  // !. Revisar en documentacion de Flutter
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class _HeaderPicoPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint(); // ! Lapiz mongol :v

    // Propiedades
    lapiz.color = const Color.fromARGB(255, 0, 0, 0);
    lapiz.style = PaintingStyle.fill; // ! pinta todo
    //lapiz.style = PaintingStyle.stroke; // ! solo el grueso de la linea
    lapiz.strokeWidth = 20;

    final path = Path();

    // lineTo trazar linea
    // moveTo mover lapiz

    /*
      ancho = width
      altura = height
    */

    double altura = size.height;
    double ancho = size.width;

    path.moveTo(0,
        0); // Esto ya no es necesario, porque siempre inicia en 0,0, ! lo pongo porque quiero
    path.lineTo(0, altura * 0.25);
    path.lineTo(ancho * 0.5, altura * 0.30);
    path.lineTo(ancho, altura * 0.25);
    path.lineTo(ancho, 0);
    path.lineTo(0,
        0); // Esto ya no es necesario, porque siempre inicia en 0,0, ! lo pongo porque quiero

    //path.lineTo(size.height * 0.25, size.width * 0.5);
    canvas.drawPath(path, lapiz);
  }

  // !. Revisar en documentacion de Flutter
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class _HeaderCurvoPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint(); // ! Lapiz mongol :v

    // Propiedades
    lapiz.color = const Color.fromARGB(255, 0, 0, 0);
    lapiz.style = PaintingStyle.fill; // ! pinta todo
    //lapiz.style = PaintingStyle.stroke; // ! solo el grueso de la linea
    lapiz.strokeWidth = 20;

    final path = Path();

    // lineTo trazar linea
    // moveTo mover lapiz

    /*
      ancho = width
      altura = height
    */

    double altura = size.height;
    double ancho = size.width;

    path.moveTo(0,
        0); // Esto ya no es necesario, porque siempre inicia en 0,0, ! lo pongo porque quiero
    path.lineTo(0, altura * 0.25);
    path.quadraticBezierTo(ancho * 0.5, altura * 0.40, ancho, altura * 0.25);
    path.lineTo(ancho, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, lapiz);
  }

  // !. Revisar en documentacion de Flutter
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class _HeaderOlaPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint(); // ! Lapiz mongol :v

    // Propiedades
    lapiz.color = const Color.fromARGB(255, 0, 0, 0);
    //lapiz.style = PaintingStyle.fill; // ! pinta todo
    lapiz.style = PaintingStyle.stroke; // ! solo el grueso de la linea
    lapiz.strokeWidth = 20;

    final path = Path();

    // lineTo trazar linea
    // moveTo mover lapiz

    /*
      ancho = width
      altura = height
    */

    double altura = size.height;
    double ancho = size.width;

    path.moveTo(0, 0); // Esto ya no es necesario, porque siempre inicia en 0,0, ! lo pongo porque quiero
    path.lineTo(0, altura * 0.30);
    path.quadraticBezierTo(ancho * 0.25, altura * 0.36, ancho * 0.5, altura * 0.30);

     //path.lineTo(ancho * 0.25, altura * 0.36);
    //path.quadraticBezierTo(ancho * 0.75, altura * 0.25, ancho, altura * 0.30);
    //path.lineTo(ancho, 0);
    //path.lineTo(0, 0);

    canvas.drawPath(path, lapiz);
  }

  // !. Revisar en documentacion de Flutter
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class _HeaderOlaPainterGradiant extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = Rect.fromCircle(
      center: const Offset(0 , 55),
      radius: 180
    );

    const Gradient gradiant = LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: <Color>[
        Color(0xff6D05E8),
        Color(0xffC012FF),
        Color(0xff6D05FA),
      ],
      stops: [
        0.2,
        0.5,
        1.0
      ]
    );

    // ! Lapiz mongol :v
    final lapiz = Paint()..shader = gradiant.createShader(rect);

    // Propiedades
    //lapiz.color = const Color.fromARGB(255, 0, 0, 0);
    //lapiz.color = Colors.red;
    lapiz.style = PaintingStyle.fill; // ! pinta todo
    //lapiz.style = PaintingStyle.stroke; // ! solo el grueso de la linea
    lapiz.strokeWidth = 20;

    final path = Path();

    // lineTo trazar linea
    // moveTo mover lapiz

    /*
      ancho = width
      altura = height
    */

    double altura = size.height;
    double ancho = size.width;

    path.moveTo(0,
        0); // Esto ya no es necesario, porque siempre inicia en 0,0, ! lo pongo porque quiero
    path.lineTo(0, altura * 0.30);
    path.quadraticBezierTo(
        ancho * 0.25, altura * 0.36, ancho * 0.5, altura * 0.30);
    path.quadraticBezierTo(ancho * 0.75, altura * 0.25, ancho, altura * 0.30);
    path.lineTo(ancho, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, lapiz);
  }

  // !. Revisar en documentacion de Flutter
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
