import 'dart:math';

import 'package:flutter/material.dart';

class RadialProgress extends StatefulWidget {
  const RadialProgress(
      {Key? key,
      required this.porcentaje,
      this.baseColor = Colors.grey,
      this.progressColor = Colors.pink,
      this.grosorBase = 4.0,
      this.grosorProgress = 10.0,
      this.requiredGradiant = false})
      : super(key: key);

  final double porcentaje;
  final Color? baseColor;
  final Color? progressColor;
  final double? grosorBase;
  final double? grosorProgress;
  final bool? requiredGradiant;

  @override
  State<RadialProgress> createState() => _RadialProgressState();
}

class _RadialProgressState extends State<RadialProgress>
    with SingleTickerProviderStateMixin {
  AnimationController? controller;
  double porcentajeAnterior = 0.0;

  @override
  void initState() {
    porcentajeAnterior = widget.porcentaje;
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller!.forward(from: 0.0);

    final diferenciaAnimar = widget.porcentaje - porcentajeAnterior;
    porcentajeAnterior = widget.porcentaje;

    return AnimatedBuilder(
      animation: controller!,
      //child: child,
      builder: (BuildContext context, Widget? child) {
        return Container(
          padding: const EdgeInsets.all(10),
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _MiRadialProgressPainter(
              porcentaje: (widget.porcentaje - diferenciaAnimar) +
                  (diferenciaAnimar * controller!.value),
              baseColor: widget.baseColor!,
              progressColor: widget.progressColor!,
              grosorBase: widget.grosorBase!,
              grosorProgress: widget.grosorProgress!,
              requiredGradiant: widget.requiredGradiant!,
            ),
          ),
        );
      },
    );

    /*    */
  }
}

class _MiRadialProgressPainter extends CustomPainter {
  _MiRadialProgressPainter({
    required this.porcentaje,
    required this.baseColor,
    required this.progressColor,
    required this.grosorBase,
    required this.grosorProgress,
    required this.requiredGradiant,
  });

  final double porcentaje;
  final Color baseColor;
  final Color progressColor;

  final double grosorBase;
  final double grosorProgress;
  final bool requiredGradiant;

  @override
  void paint(Canvas canvas, Size size) {
    // *Inicio de Circulo completado
    final paintCirculo = Paint();
    paintCirculo.strokeWidth = grosorBase;
    paintCirculo.color = baseColor;
    paintCirculo.style = PaintingStyle.stroke;
    // ancho = width | altura = height

    final double altura = size.height;
    final double ancho = size.width;

    final Offset center = Offset(ancho * 0.5, altura * 0.5);
    final double radius = min(ancho * 0.5, altura * 0.5);

    canvas.drawCircle(center, radius, paintCirculo);
    // *Fin de Circulo completado

    // *Inicio de Arco completado
    final paintArco = Paint();
    paintArco.strokeWidth = grosorProgress;

    if (requiredGradiant) {
      Rect rect = Rect.fromCircle(center: const Offset(0, 0), radius: 180);
      const Gradient gradiant = LinearGradient(
        //begin: Alignment.topCenter,
        //end: Alignment.bottomCenter,
        colors: <Color>[
          Color(0xffC012FF),
          Color(0xff6D05E8),
          Colors.red,
        ],
        //stops: [0.2, 0.5, 1.0],
      );
      paintArco.shader = gradiant.createShader(rect);
    } else {
      paintArco.color = progressColor;
    }

    paintArco.style = PaintingStyle.stroke;
    paintArco.strokeCap = StrokeCap.round; // la puntita

    // Parte que se deberá ir llenando -- 2 * pi es una vuelta completa
    double arcAngle = 2 * pi * (porcentaje / 100);

    canvas.drawArc(
        Rect.fromCircle(
            center: center,
            radius: radius), // El espacio deonde queremos ubicar el dibujo
        -pi / 2, // angulo inicial
        arcAngle, // angulo del arco
        false, // unir el arco con el inicio
        paintArco // pincel
        );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
