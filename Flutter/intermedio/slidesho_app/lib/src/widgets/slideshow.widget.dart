import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class SlideshowWidget extends StatelessWidget {
  const SlideshowWidget(
      {Key? key,
      required this.slides,
      this.pointsTop = false,
      this.noFocusColor = Colors.grey,
      this.focusColor = Colors.blue,
      this.bulletPrimary = 15,
      this.bulletSecondary = 12})
      : super(key: key);

  final List<Widget> slides;
  final bool pointsTop;
  final Color noFocusColor;
  final Color focusColor;
  final double bulletPrimary;
  final double bulletSecondary;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _SlideshowModel(),
      child: SafeArea(
        child: Center(
          child: Builder(
            builder: (BuildContext context) {
              Provider.of<_SlideshowModel>(context)
              ..focusColor = focusColor
              ..noFocusColor = noFocusColor
              ..bulletPrimary = bulletPrimary
              ..bulletSecondary = bulletSecondary;
              return _CrearEstructuraSlideshow(pointsTop: pointsTop, slides: slides);
            },
          ),
        ),
      ),
    );
  }
}

class _CrearEstructuraSlideshow extends StatelessWidget {
  const _CrearEstructuraSlideshow({
    Key? key,
    required this.pointsTop,
    required this.slides,
  }) : super(key: key);

  final bool pointsTop;
  final List<Widget> slides;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      if(pointsTop) _Dots(slides.length),
      Expanded(child: _Slides(slides)),
      if(!pointsTop) _Dots(slides.length),
    ]);
  }
}

class _Dots extends StatelessWidget {
  const _Dots(this.cantidad);

  final int cantidad;  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,      
      child: Row(        
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(cantidad, (index) => _Dot(index)),
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  const _Dot(this.index);

  final int index;    

  @override
  Widget build(BuildContext context) {
    final sliderModel = Provider.of<_SlideshowModel>(context);
    double tamano;
    Color color;
    
    if (sliderModel.currentPage >= index - 0.5 && sliderModel.currentPage < index + 0.5){
       tamano = sliderModel._bulletPrimary;
       color = sliderModel.focusColor;
    } else {
        tamano = sliderModel._bulletSecondary;
        color =  sliderModel.noFocusColor;
    }

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),      
      width: tamano,
      height: tamano,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle
      ),
    );
  }
}

class _Slides extends StatefulWidget {
  const _Slides(this.slides);

  final List<Widget> slides;

  @override
  State<_Slides> createState() => _SlidesState();
}

class _SlidesState extends State<_Slides> {

  final pageViewController = PageController();

  @override
  void initState() {
    pageViewController.addListener(() {      
      Provider.of<_SlideshowModel>(context, listen: false).currentPage = pageViewController.page!;
    });    
    super.initState();
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PageView(
        controller: pageViewController,
        physics: const BouncingScrollPhysics(),
        children: widget.slides.map((slide) => _SlideItem(slide)).toList(),        
      ),
    );
  }
}

class _SlideItem extends StatelessWidget {
  const _SlideItem(this.slide);

  final Widget slide;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: const EdgeInsets.all(30),
      child: slide
    );
  }
}

class _SlideshowModel with ChangeNotifier {
  
  double _currentPage = 0;
  Color _noFocusColor = Colors.grey;
  Color _focusColor = Colors.blue;
  double _bulletPrimary = 15;
  double _bulletSecondary = 12;


  double get currentPage => _currentPage;
  Color get noFocusColor => _noFocusColor;
  Color get focusColor => _focusColor;
  double get bulletPrimary => _bulletPrimary;
  double get bulletSecondary => _bulletSecondary;

  set currentPage(double currentPage) {
    _currentPage = currentPage;
    notifyListeners();
  }

  set noFocusColor(Color noFocusColor) {
    _noFocusColor = noFocusColor;    
  }

  set focusColor(Color focusColor) {
    _focusColor = focusColor;    
  }

  set bulletPrimary(double bulletPrimary) {
    _bulletPrimary = bulletPrimary;    
  }

  set bulletSecondary(double bulletSecondary) {
    _bulletSecondary = bulletSecondary;    
  }
  
}