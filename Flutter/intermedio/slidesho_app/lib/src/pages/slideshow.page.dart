import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:slidesho_app/src/widgets/slideshow.widget.dart';

class SlideshowPage extends StatelessWidget {
  const SlideshowPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink,
      body: Column(
        children: const [
          Expanded(child: MiShiledow()),          
        ],
      ),
    );
  }
}

class MiShiledow extends StatelessWidget {
  const MiShiledow({ Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SlideshowWidget(
      pointsTop: false,
      focusColor: Colors.white,
      bulletSecondary: 12,
      bulletPrimary: 17,
      slides: [
        SvgPicture.asset('assets/svg/slide-1.svg'),
        SvgPicture.asset('assets/svg/slide-2.svg'),
        SvgPicture.asset('assets/svg/slide-3.svg'),
        SvgPicture.asset('assets/svg/slide-4.svg'),
        SvgPicture.asset('assets/svg/slide-5.svg')
      ],
    );
  }
}
