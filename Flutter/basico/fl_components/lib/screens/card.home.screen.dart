import 'package:fl_components/widgets/custom.card.two.widget.dart';
import 'package:flutter/material.dart';

import 'package:fl_components/widgets/custom.card.one.widget.dart';

class CardScreenHome extends StatelessWidget {
  const CardScreenHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      children: const [
        CustomCardType1(),
        SizedBox(height: 20),
        CustomCardType2(
          imageUrl:
              'https://www.lenda.net/wp-content/uploads/2018/09/travel-landscape-01.jpg',
          text: 'Un Hermoso Paisaje de Montañas',
        ),
        SizedBox(height: 20),
        CustomCardType2(
          imageUrl:
              'https://www.mickeyshannon.com/photos/landscape-photography.jpg',
          text: 'Un hermoso paisaje de Nevados',
        ),
        SizedBox(height: 20),
        CustomCardType2(
          imageUrl:
              'http://www.solofondos.com/wp-content/uploads/2016/04/mountain-landscape-wallpaper.jpg',
          text: 'Un hermoso Paisaje de meditacion',
        ),
        SizedBox(height: 20),
        CustomCardType2(
            imageUrl:
                'http://www.defondos.com/bulkupload/imagenes-de-lagos/Paisajes/Lagos/Bello%20Paisaje_800.jpg'),
        SizedBox(height: 20),
        CustomCardType2(
            imageUrl:
                'https://www.lenda.net/wp-content/uploads/2018/09/travel-landscape-01.jpg')
      ],
    ));
  }
}
