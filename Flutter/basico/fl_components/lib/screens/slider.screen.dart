import 'package:fl_components/themes/app.theme.ligth.dart';
import 'package:flutter/material.dart';

class SliderScreen extends StatefulWidget {
  const SliderScreen({Key? key}) : super(key: key);

  @override
  State<SliderScreen> createState() => _SliderScreenState();
}

class _SliderScreenState extends State<SliderScreen> {
  double _sliderValue = 100;
  bool _slide = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Slider Screen')),
      body: Column(children: [
        Slider.adaptive(
            min: 50,
            max: 400,
            activeColor: AppThemeLigth.primary,
            divisions: 10,
            value: _sliderValue,
            onChanged: _slide
                ? (value) {
                    _sliderValue = value;
                    setState(() {});
                    //print(value);
                  }
                : null),
        Checkbox(
            value: _slide,
            onChanged: (value) {
              _slide = value ?? true;
              setState(() {});
            }),
        CheckboxListTile(
            value: _slide,
            activeColor: AppThemeLigth.primary,
            title: const Text('Habilitar Slider'),
            onChanged: (value) {
              _slide = value ?? true;
              setState(() {});
            }),
        Switch.adaptive(
            value: _slide,
            activeColor: AppThemeLigth.primary,
            onChanged: (value) {
              _slide = value;
              setState(() {});
            }),
        SwitchListTile.adaptive(
            value: _slide,
            title: const Text('Habilitar Slider'),
            activeColor: AppThemeLigth.primary,
            onChanged: (value) {
              _slide = value;
              setState(() {});
            }),
        const AboutListTile(),
        Expanded(
          child: SingleChildScrollView(
            child: Image(
              image: const NetworkImage(
                  'https://c.tenor.com/Nmy4xsFrDDAAAAAC/crying-eric-cartman.gif'),
              fit: BoxFit.contain,
              width: _sliderValue,
            ),
          ),
        )
      ]),
    );
  }
}
