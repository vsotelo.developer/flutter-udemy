import 'package:fl_components/models/models.dart';
import 'package:fl_components/router/app.route.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<MenuOptions> menuOptions = AppRoutes.menuOptions;

    return Scaffold(
        appBar: AppBar(title: const Text('Componentes Flutter')),
        body: ListView.separated(
            itemBuilder: (context, index) => ListTile(
                  leading: Icon(menuOptions[index].inconData),
                  title: Text(menuOptions[index].name),
                  onTap: () {
                    if (menuOptions[index].route != 'home') {
                      Navigator.pushNamed(context, menuOptions[index].route);
                    }
                  },
                ),
            separatorBuilder: (_, __) => const Divider(),
            itemCount: menuOptions.length));
  }
}
