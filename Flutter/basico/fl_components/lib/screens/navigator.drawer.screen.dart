// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:fl_components/screens/card.home.screen.dart';
import 'package:flutter/material.dart';

// Press the Navigation Drawer button to the left of AppBar to show
// a simple Drawer with two items.
class NavDrawerDemo extends StatelessWidget {
  const NavDrawerDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const drawerHeader = UserAccountsDrawerHeader(
      accountName: Text(
        'Victor Andres Sotelo Meza',
      ),
      accountEmail: Text(
        'vsotelo@linkfast.io',
      ),
      currentAccountPicture:
          CircleAvatar(backgroundImage: AssetImage('assets/img/vsotelo.jpg')),
    );
    final drawerItems = ListView(
      children: [
        drawerHeader,
        ListTile(
          title: const Text(
            'Home',
          ),
          leading: const Icon(Icons.home),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text(
            'Alerts',
          ),
          leading: const Icon(Icons.report),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Home',
        ),
      ),
      body: const CardScreenHome(),
      drawer: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: Drawer(
          child: drawerItems,
        ),
      ),
    );
  }
}
