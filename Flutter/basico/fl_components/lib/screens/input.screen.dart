import 'package:fl_components/widgets/custom.field.widget.dart';
import 'package:flutter/material.dart';

class InputScreen extends StatelessWidget {
  const InputScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> myFormKey = GlobalKey<FormState>();

    final Map<String, String> formValues = {
      'firstName': 'Fernando',
      'lastName': 'Herrera',
      'email': 'vsotelo@linkfast.io',
      'password': '123456',
      'role': 'Admin'
    };

    return Scaffold(
        appBar: AppBar(title: const Text('Inputs y Forms')),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Form(
            key: myFormKey,
            child: Column(children: [
              CustomInputField(
                  formProperty: 'firstName',
                  formValues: formValues,
                  labelText: 'Nombre',
                  hintText: 'Nombre de usuario',
                  icon: Icons.group_outlined),
              const SizedBox(height: 30),
              CustomInputField(
                  formProperty: 'lastName',
                  formValues: formValues,
                  labelText: 'Apellido',
                  hintText: 'Apellido de usuario',
                  icon: Icons.group_outlined),
              const SizedBox(height: 30),
              CustomInputField(
                  formProperty: 'email',
                  formValues: formValues,
                  labelText: 'Correo',
                  hintText: 'Correo de usuario',
                  icon: Icons.email_outlined,
                  keyboardType: TextInputType.emailAddress),
              const SizedBox(height: 30),
              CustomInputField(
                  formProperty: 'password',
                  formValues: formValues,
                  labelText: 'Contraseña',
                  hintText: 'Correo de usuario',
                  icon: Icons.email_outlined,
                  obscureText: true),
              const SizedBox(height: 30),
              DropdownButtonFormField<String>(
                  items: const [
                    DropdownMenuItem(value: 'Admin', child: Text('Admin')),
                    DropdownMenuItem(
                        value: 'SuperUser', child: Text('Super User')),
                    DropdownMenuItem(
                        value: 'Developer', child: Text('Developer')),
                    DropdownMenuItem(
                        value: 'Jr. Developer', child: Text('Jr. Developer')),
                  ],
                  onChanged: (value) {
                    print(value);
                    formValues['role'] = value ?? 'Admin';
                  }),
              //const SizedBox(height: 30),
              ElevatedButton(
                  child: const SizedBox(
                      width: double.infinity,
                      child: Center(child: Text('Guardar'))),
                  onPressed: () {
                    FocusScope.of(context)
                        .requestFocus(FocusNode()); // Teclaudo minimizado

                    if (!myFormKey.currentState!.validate()) {
                      print('Formulario no valido');
                      return;
                    }
                    print(formValues);
                  })
            ]),
          ),
        )));
  }
}
