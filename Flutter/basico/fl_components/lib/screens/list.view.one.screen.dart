import 'package:flutter/material.dart';

class ListViewOneScreen extends StatelessWidget {
  const ListViewOneScreen({Key? key}) : super(key: key);

  final List<String> options = const [
    'Megaman',
    'Metal Gear',
    'Super Smash',
    'Final Fantasy'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Opciones'),
        ),
        body: ListView(
          children: <Widget>[
            ...options
                .map(
                  (name) => ListTile(
                      title: Text(name),
                      trailing: const Icon(Icons.arrow_forward_ios_outlined)),
                )
                .toList()
          ],
        ));
  }
}
