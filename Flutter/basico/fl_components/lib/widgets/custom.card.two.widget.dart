import 'package:fl_components/themes/app.theme.ligth.dart';
import 'package:flutter/material.dart';

class CustomCardType2 extends StatelessWidget {
  const CustomCardType2({Key? key, required this.imageUrl, this.text})
      : super(key: key);

  final String imageUrl;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      elevation: 30,
      shadowColor: AppThemeLigth.primary.withOpacity(0.3),
      child: Column(children: [
        FadeInImage(
            image: NetworkImage(imageUrl),
            placeholder: const AssetImage('assets/img/loading.gif'),
            width: double.infinity,
            height: 230,
            fit: BoxFit.cover //fadeInDuration: Duration(milliseconds: 500)
            ),
        if (text != null)
          Container(
              alignment: AlignmentDirectional.centerEnd,
              padding: const EdgeInsets.only(right: 20, top: 20, bottom: 10),
              child: Text(text ?? 'Text empty'))
      ]),
    );
  }
}
