import 'package:flutter/material.dart';

import '../themes/app.theme.ligth.dart';

class CustomCardType1 extends StatelessWidget {
  const CustomCardType1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(children: [
        ListTile(
          leading:
              Icon(Icons.photo_album_outlined, color: AppThemeLigth.primary),
          title: const Text(
            'Loren Ipsum',
            style: TextStyle(height: 3),
          ),
          subtitle: const Text(
              'Ez Velit labore ipsum ea et Lorem non. Do anim labore irure nisi ut reprehenderit deserunt magna. Eu non deserunt et ullamco aute in irure eiusmod. Eu deserunt minim voluptate elit aliquip laboris. Ad irure irure fugiat nisi culpa.'),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(onPressed: () {}, child: const Text('Cancel')),
              TextButton(onPressed: () {}, child: const Text('Ok'))
            ],
          ),
        )
      ]),
    );
  }
}
