import 'package:flutter/material.dart' show IconData, Widget;

class MenuOptions {
  final String route;
  final IconData inconData;
  final String name;
  final Widget widget;

  MenuOptions(
      {required this.route,
      required this.inconData,
      required this.name,
      required this.widget});
}
