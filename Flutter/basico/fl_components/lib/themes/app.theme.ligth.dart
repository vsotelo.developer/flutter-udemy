import 'package:flutter/material.dart';

class AppThemeLigth {
  static Color primary = Colors.indigo;

  static final ThemeData ligthTheme = ThemeData.light().copyWith(
      // Primery Color
      primaryColor: primary,
      // AppBar Theme
      appBarTheme: AppBarTheme(color: primary, elevation: 0),
      // Icon Theme
      iconTheme: IconThemeData(color: primary),
      // TextButton Theme
      textButtonTheme:
          TextButtonThemeData(style: TextButton.styleFrom(primary: primary)),
      // FloatingAcctionButton Theme
      floatingActionButtonTheme:
          FloatingActionButtonThemeData(backgroundColor: primary),
      // ElevatedButtons Theme
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              primary: AppThemeLigth.primary,
              shape: const StadiumBorder(),
              elevation: 0)),
      // Input Decoration Theme
      inputDecorationTheme: InputDecorationTheme(
          floatingLabelStyle: TextStyle(color: primary),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: primary),
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: primary),
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10))),
          border: const OutlineInputBorder(
              //borderSide: BorderSide(color: primary),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10)))));
}
