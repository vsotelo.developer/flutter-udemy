import 'package:fl_components/models/models.dart';
import 'package:fl_components/screens/animated.screen.dart';
import 'package:fl_components/screens/avatar.screen.dart';
import 'package:fl_components/screens/input.screen.dart';
import 'package:fl_components/screens/list.view.builder.screen.dart';
import 'package:fl_components/screens/navigator.drawer.screen.dart';
import 'package:fl_components/screens/slider.screen.dart';
import 'package:flutter/material.dart';

import '../screens/screens.dart';

class AppRoutes {
  static const String initialRoutes = 'home';

  static final List<MenuOptions> menuOptions = <MenuOptions>[
    MenuOptions(
        route: 'home',
        name: 'Home Screen',
        widget: const HomeScreen(),
        inconData: Icons.home),
    MenuOptions(
        route: 'listview1',
        name: 'ListView1 Screen',
        widget: const ListViewOneScreen(),
        inconData: Icons.list),
    MenuOptions(
        route: 'listview2',
        name: 'ListView2 Screen',
        widget: const ListViewTwoScreen(),
        inconData: Icons.list_alt),
    MenuOptions(
        route: 'alert',
        name: 'Alert Screen',
        widget: const AlertScreen(),
        inconData: Icons.add_alert),
    MenuOptions(
        route: 'card',
        name: 'Card Screen',
        widget: const CardScreen(),
        inconData: Icons.card_giftcard),
    MenuOptions(
        route: 'navigator',
        name: 'Navigator',
        widget: const NavDrawerDemo(),
        inconData: Icons.account_balance_wallet_outlined),
    MenuOptions(
        route: 'avatar',
        inconData: Icons.supervised_user_circle_outlined,
        name: 'Avatar',
        widget: const AvatarScreen()),
    MenuOptions(
        route: 'animated',
        inconData: Icons.play_circle_fill_outlined,
        name: 'Animated Container',
        widget: const AnimatedScreen()),
    MenuOptions(
        route: 'input',
        inconData: Icons.input_rounded,
        name: 'Text input',
        widget: const InputScreen()),
    MenuOptions(
        route: 'slider',
        inconData: Icons.slideshow_outlined,
        name: 'Slider Screen',
        widget: const SliderScreen()),
    MenuOptions(
        route: 'listbuilder',
        inconData: Icons.build_circle_outlined,
        name: 'ListBuiler',
        widget: const ListBuilderScreen())
  ];

  /*static Map<String, Widget Function(BuildContext)> routes = {
    'home': (BuildContext context) => const HomeScreen(),
    'listview1': (BuildContext context) => const ListViewOneScreen(),
    'listview2': (BuildContext context) => const ListViewTwoScreen(),
    'alert': (BuildContext context) => const AlertScreen(),
    'card': (BuildContext context) => const CardScreen()
  };
  */

  static Map<String, Widget Function(BuildContext)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};
    for (final option in menuOptions) {
      appRoutes.addAll({option.route: (BuildContext context) => option.widget});
    }

    return appRoutes;
  }

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (context) => const AlertScreen());
  }
}
