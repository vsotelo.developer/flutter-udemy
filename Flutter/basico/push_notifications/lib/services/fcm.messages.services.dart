//! SHA1: C0:52:DF:87:22:E6:6E:9E:A6:29:72:20:24:06:58:5D:83:D9:EC:79

import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationServices {
  static FirebaseMessaging messaging = FirebaseMessaging.instance;
  static String? token;

  static final StreamController<String> _messageStream =
      StreamController.broadcast();

  static Stream<String> get messageStream => _messageStream.stream;

  //! Inicio de config token y toda la wea
  static Future initializeApp() async {
    // Push Notifications
    await Firebase.initializeApp();
    token = await FirebaseMessaging.instance.getToken();
    print('Token: $token');

    // Handlers
    FirebaseMessaging.onMessage.listen(_onMessageHandler);

    FirebaseMessaging.onBackgroundMessage(_onBackgroundHandler);

    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenApp);

    // Local Notifications
  }

  //!App abierta
  static Future _onMessageHandler(RemoteMessage message) async {
    print(message.data);
    _messageStream.add(message.data['product'] ?? 'No data');
  }

  //!App minimizada
  static Future _onBackgroundHandler(RemoteMessage message) async {
    print(message.data);
    _messageStream.add(message.data['product'] ?? 'No data');
  }

  // !App cerrada
  static Future _onMessageOpenApp(RemoteMessage message) async {
    print(message.data);
    _messageStream.add(message.data['product'] ?? 'No data');
  }

  // !Nunca se llama pero se agrega para ver como se cierra ps :,v
  static closeStreams() {
    _messageStream.close();
  }
}

/*
  REST:
  URL: https://fcm.googleapis.com/fcm/send
  METHOD: POST
  HEADERS:
    Authorization: key={token Cloud Messaging}
  BODY: JSON  
  {
    "notification":{
        "body": "Texto de la notification!!",
        "title": "Titulo"        
    },
    "priority": "high",
    "data": {
        "product": "Awa"
    },
    "to": "fAupwEDFSCei4_wSNhryWb:APA91bH7z-d9mHWEV9YWnHvQuld1N-cig6FC9rqUfGpoVkrAkw9lQ2DURc9y8SjgdlBXCBJ3FqhZdSxB3sr5W0vQ1a3kANWtXZpKBxd_X9E8SA_xO-hgDXsMGWfpoXpeitOsfgY6A85G"
}
*/ 