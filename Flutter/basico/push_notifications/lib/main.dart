import 'package:flutter/material.dart';
import 'package:push_notifications/screens/home.screen.dart';
import 'package:push_notifications/screens/message.screen.dart';
import 'package:push_notifications/services/fcm.messages.services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await PushNotificationServices.initializeApp();

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<ScaffoldMessengerState> messengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    PushNotificationServices.messageStream.listen((message) {
      navigatorKey.currentState?.pushNamed('message', arguments: message);

      final snackBar = SnackBar(content: Text(message));
      messengerKey.currentState?.showSnackBar(snackBar);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: "home",
      navigatorKey: navigatorKey, //Navegar
      scaffoldMessengerKey: messengerKey, // Snacks
      routes: {
        'home': (_) => const HomeScreen(),
        'message': (_) => const MessageScreen(),
      },
    );
  }
}
