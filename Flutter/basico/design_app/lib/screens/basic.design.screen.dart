import 'package:flutter/material.dart';

class BasicDesignScreen extends StatelessWidget {
  const BasicDesignScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            // Image
            const Image(image: AssetImage('assets/landscape.jpg')),
            // Title
            const TitleWidget(),
            // Button Section
            const ButtonSectionWidget(),
            // Description
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: const Text(
                'Sunt laboris laboris ex ad officia sunt esse proident nostrud quis ea dolor esse. Sunt ea magna eu et anim duis ea duis proident officia laborum dolor nostrud dolore. Id deserunt cillum esse aute ut ullamco deserunt dolore aliqua minim.',
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: const Text(
                'Sunt laboris laboris ex ad officia sunt esse proident nostrud quis ea dolor esse. Sunt ea magna eu et anim duis ea duis proident officia laborum dolor nostrud dolore. Id deserunt cillum esse aute ut ullamco deserunt dolore aliqua minim.',
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: const Text(
                'Sunt laboris laboris ex ad officia sunt esse proident nostrud quis ea dolor esse. Sunt ea magna eu et anim duis ea duis proident officia laborum dolor nostrud dolore. Id deserunt cillum esse aute ut ullamco deserunt dolore aliqua minim.',
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: const Text(
                'Sunt laboris laboris ex ad officia sunt esse proident nostrud quis ea dolor esse. Sunt ea magna eu et anim duis ea duis proident officia laborum dolor nostrud dolore. Id deserunt cillum esse aute ut ullamco deserunt dolore aliqua minim.',
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: const Text(
                'Sunt laboris laboris ex ad officia sunt esse proident nostrud quis ea dolor esse. Sunt ea magna eu et anim duis ea duis proident officia laborum dolor nostrud dolore. Id deserunt cillum esse aute ut ullamco deserunt dolore aliqua minim.',
                textAlign: TextAlign.justify,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TitleWidget extends StatelessWidget {
  const TitleWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text('One Piece - El Mejor anime del Mundo!',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )),
              SizedBox(height: 5),
              Text(
                'Si no lo has visto eres gil',
                style: TextStyle(color: Colors.black45),
              ),
            ],
          ),
          Expanded(child: Container()),
          const Icon(Icons.star, color: Colors.red),
          const Text('41'),
        ],
      ),
    );
  }
}

class ButtonSectionWidget extends StatelessWidget {
  const ButtonSectionWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: const [
          MiniButtonSection(text: 'CALL', iconData: Icons.call),
          MiniButtonSection(text: 'ROUTE', iconData: Icons.route),
          MiniButtonSection(text: 'SHARE', iconData: Icons.share),
        ],
      ),
    );
  }
}

class MiniButtonSection extends StatelessWidget {
  const MiniButtonSection({
    Key? key,
    required this.text,
    required this.iconData,
  }) : super(key: key);

  final String text;
  final IconData iconData;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          iconData,
          size: 50,
          color: Colors.blue,
        ),
        const SizedBox(height: 5),
        Text(text)
      ],
    );
  }
}
