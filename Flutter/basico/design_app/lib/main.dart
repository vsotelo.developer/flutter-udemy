import 'package:flutter/material.dart';

import 'package:design_app/screens/basic.design.screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'basic-design',
      routes: {'basic-design': (_) => const BasicDesignScreen()},
    );
  }
}
