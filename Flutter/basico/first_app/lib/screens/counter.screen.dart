

import 'package:flutter/material.dart';

class CounterScreen extends StatefulWidget{
    
  const CounterScreen({Key? key}) : super(key: key);

  @override
  State<CounterScreen> createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {


  int counter = 0;

  @override
  Widget build(BuildContext context) {

    const double fontSize30 = 30;

    void increase(){
      counter++;
      setState(() {});
    }

    void decrease(){
      counter--;
      setState(() {});
    }

    void reset(){
      counter = 0;
      setState(() {});
    }
    
    return Scaffold(  
      appBar: AppBar(
        title: const Text('Counter Screen'),
        elevation: 0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,                
          children: <Widget> [
            const Text('Número de Clicks', style: TextStyle(fontSize: fontSize30)),
            Text('$counter', style: const TextStyle(fontSize: fontSize30))
          ],
        ),        
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,      
      floatingActionButton: CustoFloatingActionButton(
        increase: increase,
        decrease: decrease,
        reset: reset
      ),         
    );    
  }
}

class CustoFloatingActionButton extends StatelessWidget {
  
  final Function increase;
  final Function decrease;
  final Function reset;

  const CustoFloatingActionButton({
    Key? key, required this.increase, required this.decrease, required this.reset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        FloatingActionButton(
          child: const Icon(Icons.plus_one),
           onPressed: () => increase(),
        ),          
        FloatingActionButton(
          child: const Icon(Icons.exposure_minus_1),
          onPressed:  () => decrease(),
        ),             
        FloatingActionButton(
          child: const Icon(Icons.replay_outlined),
          onPressed:  () => reset(),
        ),                     
      ],        
    );
  }
}

/*onPressed: () {  
    counter++;                          
    setState(() {});
  },*/