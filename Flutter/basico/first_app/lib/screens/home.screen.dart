

import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget{
    
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    const double fontSize30 = 30;
    int counter = 0;
    return Scaffold(  
      appBar: AppBar(
        title: const Text('Home Screen'),
        elevation: 0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,                
          children: <Widget> [
            const Text('Número de Clicks', style: TextStyle(fontSize: fontSize30)),
            Text('$counter', style: const TextStyle(fontSize: fontSize30))
          ],
        ),        
      ),
      //floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.plus_one),
        onPressed: () { 
            counter++;
            print(counter);
         },
      ),
    );    
  }
}