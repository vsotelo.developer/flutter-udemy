
import 'package:first_app/screens/counter.screen.dart';
import 'package:flutter/material.dart';

void main() {
   runApp(const FirstApp());
}

class FirstApp extends StatelessWidget{

  const FirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: CounterScreen() // --> Widget view
    );      
  }

}