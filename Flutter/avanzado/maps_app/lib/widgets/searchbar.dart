import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_app/blocs/blocks.dart';
import 'package:maps_app/delegates/search_destination_delegate.dart';
import 'package:maps_app/models/models.dart';

import '../helpers/helpers.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      builder: (context, state) {
        return state.displayManualMarker 
            ? const SizedBox() 
            : FadeInDown(
              duration: const Duration(milliseconds: 300),
              child: const _SearchBarBody()
            );
      },
    );
  }
}

class _SearchBarBody extends StatelessWidget {
  const _SearchBarBody({Key? key}) : super(key: key);

  void onSeachResult(BuildContext context, SearchResult searchResult) async {

    final searchBloc = BlocProvider.of<SearchBloc>(context);


    if(searchResult.manual){
      searchBloc.add(OnActivatedManualMarkerEvent());
      return;

    } else if(searchResult.place != null) {

        final locationBloc = BlocProvider.of<LocationBloc>(context);
        final mapBloc = BlocProvider.of<MapBloc>(context);

        final start = locationBloc.state.lastKnowLocation;
        if (start == null) return;                                
        final end = LatLng(searchResult.place!.center[1],searchResult.place!.center[0]);
        
        showLoandingMessage(context);                 

        RouteDestination routeDestination = await searchBloc.getCoorsStartToEnd(start, end);
        await mapBloc.drawRoutePolyline(routeDestination);

        searchBloc.add(OnDeactivedManualMarkerEvent());

        Navigator.pop(context);  
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        padding: const EdgeInsets.symmetric(horizontal: 15),        
        width: double.infinity,        
        child: GestureDetector(
          onTap: () async {            
            final result = await showSearch(context: context, delegate: SearchDestinationDelegate());
            if(result == null) return;
            onSeachResult(context, result);
          },
          child: Container (
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 13),
            child: const Text('¿Dónde quieres ir?', style: TextStyle(color: Colors.black87)),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
              boxShadow: const [
                BoxShadow( color: Colors.black12, blurRadius: 5, offset: Offset(0, 5))
              ]
            ),
          ),
        ),        
      ),
    );
  }
}