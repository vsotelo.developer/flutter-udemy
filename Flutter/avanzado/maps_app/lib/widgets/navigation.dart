import 'package:flutter/material.dart';

class CustomNavigatiomDrawer extends StatelessWidget {
  const CustomNavigatiomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        child: Column(
          children: [
            const _HeaderNavigator(),
            const SizedBox(height: 5),
            const Divider(),
            Expanded(child: _ListOptions()),
            const Divider(),
            const _ButtonDarkMode(),
          ],
        ),
      ),
    );
  }
}

class _ButtonDarkMode extends StatelessWidget {
  const _ButtonDarkMode({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return SafeArea(
      bottom: true,
      top: false,
      right: false,
      left: false,
      child: ListTile(
        leading: const Icon(Icons.lightbulb_outline, color: Colors.black),
        title: const Text('Dark Mode'),
        trailing: Switch.adaptive(
          value: true,
          activeColor: Colors.black,
          onChanged: (value) {

            //= value;
          },
        ),
      ),
    );
  }
}

// ignore: slash_for_doc_comments
/**
 *  * INIT WIDGETS PRIVATES
 *
**/

// * Header Navigator
class _HeaderNavigator extends StatelessWidget {
  const _HeaderNavigator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.height * 0.1,
              height: 80,
              child: const CircleAvatar(
                backgroundColor: Colors.black,
                child: Text('VS', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold, color: Colors.white)),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20),
              //width: double.infinity,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      'Hola,',
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 20),
                    ),
                    Text('Victor Sotelo', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}

class _ListOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      separatorBuilder: (_, __) => const Divider(
        color: Colors.black,
      ),
      itemCount: 2,
      itemBuilder: (_, i) => ListTile(
        leading: const Icon(Icons.home),
        title: const Text('Home', style: TextStyle(fontSize: 15)),
        trailing: Icon(Icons.chevron_right, color: Colors.black.withOpacity(0.5)),
        onTap: () {          
        },
      ),
    );
  }
}
