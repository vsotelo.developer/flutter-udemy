import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_app/blocs/blocks.dart';

class MapView extends StatelessWidget {
  const MapView({Key? key, required this.initialLocation, required this.polylines, required this.markers}) : super(key: key);

  final LatLng  initialLocation;
  final Set<Polyline> polylines;
  final Set<Marker> markers;

  @override
  Widget build(BuildContext context) {

    final mapBloc = BlocProvider.of<MapBloc>(context);

    final CameraPosition initialCameraPosition = CameraPosition(
        //bearing: 192.8334901395799,
        target: initialLocation,
        //tilt: 59.440717697143555,
        zoom: 15
    );

    final size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width,
      height: size.height,
      child: Listener(
        onPointerMove: (pinterMoveEvent) => mapBloc.add(OnStopFollowingUserEvent()),
        child: GoogleMap(
          initialCameraPosition: initialCameraPosition,
          compassEnabled: false,
          myLocationEnabled: true,
          zoomControlsEnabled: false,
          myLocationButtonEnabled: false,
          onMapCreated: (controller) => mapBloc.add(OnMapInitializedEvent(controller)),
          polylines: polylines,
          onCameraMove: (position) => mapBloc.mapCenter = position.target,
          markers: markers,
          //onCameraMove: 
        ),
      ),
    );
  }
}
