import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_polyline_algorithm/google_polyline_algorithm.dart';
import 'package:maps_app/models/models.dart';
import 'package:maps_app/services/services.dart';


part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {

  TrafficService trafficService;

  SearchBloc({required this.trafficService}) : super(const SearchState()) {

    on<OnActivatedManualMarkerEvent>((event, emit) => emit(state.copyWith(displayManualMarker: true)));
    on<OnDeactivedManualMarkerEvent>((event, emit) => emit(state.copyWith(displayManualMarker: false)));
    on<OnNewPlacesFoundEvent>((event, emit) => emit(state.copyWith(places: event.places)));
    on<AddtoHistoryEvent>((event, emit) => emit(state.copyWith(history: [event.place, ...state.history])));
  
  }

  Future<RouteDestination> getCoorsStartToEnd(LatLng start, LatLng end) async {
    final traffictResponse = await trafficService.getCoorsStartToEnd(start, end);

    final endPlace = await trafficService.getInformationByCoors(end);

    final duration = traffictResponse.routes[0].duration;
    final distance = traffictResponse.routes[0].distance;

    final geometry = traffictResponse.routes[0].geometry;   

    // Decodificar
    final points = decodePolyline(geometry, accuracyExponent: 6);

    final latLngList = points.map((coor) => LatLng(coor[0].toDouble(), coor[1].toDouble())).toList();

    return RouteDestination(
      points: latLngList,
      duration: duration,
      distance: distance,
      endPlace: endPlace,
    );
  }  

  Future getPlacesByQuery(LatLng proximity, String query) async {
    final newPlaces = await trafficService.getResultsByQuery(proximity, query);

    add(OnNewPlacesFoundEvent(newPlaces));   
    
  }
}

