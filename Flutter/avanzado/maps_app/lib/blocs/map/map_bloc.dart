import 'dart:convert';
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_app/blocs/blocks.dart';
import 'package:maps_app/helpers/helpers.dart';
import 'package:maps_app/models/models.dart';
import 'package:maps_app/themes/map.ligth.dart';

part 'map_event.dart';
part 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {

  final LocationBloc locationBloc;
  
  GoogleMapController? _mapController;
  LatLng? mapCenter; 

  StreamSubscription<LocationState>? locationStateSubscription;
  
  MapBloc({required this.locationBloc}) : super(const MapState()) {

    on<OnMapInitializedEvent>(_onInitMao);
    on<OnStartFollowingUserEvent>(_onStartFollowingUser);
    on<OnStopFollowingUserEvent>((event, emit) => emit(state.copyWith(isFollowingUser: false)));
    on<UpdateUserPolylinesEvent>(_onPolylineNewPoint);
    on<OnTogleeUserRoute>((event, emit) => emit(state.copyWith(showMyRoute: !state.showMyRoute)));
    on<DisplayPolylinesEvent>((event, emit) => emit(state.copyWith(polylines: event.polylines, markers: event.markers)));


    locationStateSubscription = locationBloc.stream.listen((locationState) {

      if(locationState.lastKnowLocation != null ){
        add(UpdateUserPolylinesEvent(locationState.myLocationHistory));
      }

      if (!state.isFollowingUser) return;
      if (locationState.lastKnowLocation == null) return;
      
      moveCamera(locationState.lastKnowLocation!);

    });   
  }

  void _onInitMao(OnMapInitializedEvent event, Emitter<MapState> emit) {
    _mapController = event.controller;
    _mapController!.setMapStyle(jsonEncode(mapLigthTheme));
    emit(state.copyWith(isMapInitialized: true));
  }

  void _onStartFollowingUser(OnStartFollowingUserEvent event, Emitter<MapState> emit){      
      emit(state.copyWith(isFollowingUser: true));

      if (locationBloc.state.lastKnowLocation == null) return;
      moveCamera(locationBloc.state.lastKnowLocation!);

  }

  void moveCamera(LatLng newLocation) {
    final cameraUpdate = CameraUpdate.newLatLng(newLocation);
    _mapController?.animateCamera(cameraUpdate);
  }

  void _onPolylineNewPoint(UpdateUserPolylinesEvent event, Emitter<MapState> emit){
    
    final myRoute = Polyline(
      polylineId: const PolylineId('myRoute'),
      color: Colors.black,
      width: 5,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
      points: event.userLocations,
    );

    final currentPolylines = Map<String, Polyline>.from(state.polylines);
    currentPolylines['myRoute'] = myRoute;

    emit(state.copyWith(polylines: currentPolylines));

  }

  Future<void> drawRoutePolyline(RouteDestination routeDestination) async {

    final myRoute = Polyline(
      polylineId: const PolylineId('route'),
      color: Colors.black,
      points: routeDestination.points,
      width: 5,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,      
    );

    double kms = routeDestination.distance / 1000;
    kms = (kms * 100).floorToDouble();
    kms /= 100;

    double tripDuration = (routeDestination.duration / 60).floorToDouble();

    // Custom Markers

    final startMarkerInit = await getStartCustomMarker(tripDuration.toInt(), 'Mi Ubicación.');
    final endMarkerEnd = await getEndCustomMarker(kms.toInt(), routeDestination.endPlace.text);

    
    final startMarker = Marker(
      markerId: const MarkerId('start'),
      position: routeDestination.points.first,
      icon: startMarkerInit,
      anchor: const Offset(0.1, 1)
      //anchor: const Offset(0,0),
      /*infoWindow: InfoWindow(
        title: 'Inicio',
        snippet: 'Ksm: $kms, duration: $tripDuration'
      )*/
    );

    final endMarker = Marker(
      markerId: const MarkerId('end'),
      position: routeDestination.points.last,
      icon: endMarkerEnd,
      /*infoWindow: InfoWindow(
        title: routeDestination.endPlace.text,
        snippet: routeDestination.endPlace.placeName
      )*/      
    );    
    
    final currentPolylines = Map<String, Polyline>.from(state.polylines);
    currentPolylines['route'] = myRoute;

    final currentMarkers = Map<String, Marker>.from(state.markers);
    currentMarkers['start'] = startMarker;
    currentMarkers['end'] = endMarker;

    add(DisplayPolylinesEvent(currentPolylines, currentMarkers));

    await Future.delayed(const Duration(milliseconds: 300));

    //_mapController?.showMarkerInfoWindow(const MarkerId('start'));

  }

  @override
  Future<void> close() {
    locationStateSubscription?.cancel();
    return super.close();
  }

}
