import 'package:dio/dio.dart';

class PlacesInterceptor extends Interceptor {

  String accesToken = 'pk.eyJ1IjoidnNvdGVsbyIsImEiOiJjbDE3c3BydmswYmhwM2RsaWlhNGtzc3V3In0.w8iZN9QB0_YbufUvGIuR0g';
  
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    
    options.queryParameters.addAll({
      'access_token': accesToken,
      'country': 'pe',
      'language': 'es',     
    });

    super.onRequest(options, handler);
  }
}
