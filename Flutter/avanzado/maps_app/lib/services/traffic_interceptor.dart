import 'package:dio/dio.dart';

class TraffictInterceptor extends Interceptor {

  String accesToken = 'pk.eyJ1IjoidnNvdGVsbyIsImEiOiJjbDE3c3BydmswYmhwM2RsaWlhNGtzc3V3In0.w8iZN9QB0_YbufUvGIuR0g';

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {

    options.queryParameters.addAll({
      'alternatives' : true,
      'geometries': 'polyline6',
      'overview': 'simplified',
      'steps': false,
      'access_token': accesToken,
    });

    super.onRequest(options, handler);
  }

}