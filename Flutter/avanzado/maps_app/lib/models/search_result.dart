

import 'package:google_maps_flutter/google_maps_flutter.dart' show LatLng;
import 'package:maps_app/models/models.dart';

class SearchResult {

  final bool cancel;
  final bool manual;  
  final Feature? place;

  SearchResult({
     required this.cancel,
     this.manual = false,     
     this.place,
  });

  @override
  String toString() {
    return '{ cancel: $cancel, manual : $manual }';
  }

}