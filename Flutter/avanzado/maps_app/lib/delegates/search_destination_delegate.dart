

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maps_app/blocs/blocks.dart';
import 'package:maps_app/models/models.dart';



class SearchDestinationDelegate extends SearchDelegate<SearchResult>{

  SearchDestinationDelegate(): super (
    searchFieldLabel: 'Buscar...'

  );
  
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: (){
          query = '';
        },        
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        icon: const Icon(Icons.arrow_back_ios),
        onPressed: (){
          close(context, SearchResult(cancel: true));
        },        
      );
  }

  @override
  Widget buildResults(BuildContext context) {   

    final searchBloc = BlocProvider.of<SearchBloc>(context);
    final proximity = BlocProvider.of<LocationBloc>(context).state.lastKnowLocation!;

    searchBloc.getPlacesByQuery(proximity, query);

    return BlocBuilder<SearchBloc, SearchState>(
      builder: (context, state) {        
        final places = state.places;        
        return ListView.separated(
          itemCount: places.length,
          separatorBuilder: (_, __) => const Divider(),
          itemBuilder: (_, i) {
            final place = places[i]  ;
            return ListTile(
              title: Text(place.text),
              subtitle: Text(place.placeName),
              leading: const Icon(Icons.place_outlined, color: Colors.black),  
              onTap: (){
                searchBloc.add(AddtoHistoryEvent(place));
                close(context, SearchResult(cancel: false, manual: false, place: place));
              },
            );
          }, 
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {

     final history =BlocProvider.of<SearchBloc>(context).state.history;

     return ListView(
       children: [
         ListTile(
           title: const Text('Colocar la ubicacion manualmente', style: TextStyle(color: Colors.black),),
           leading: const Icon(Icons.location_on_outlined, color: Colors.black),     
           onTap: () {
              close(context, SearchResult(cancel: false, manual: true));
           },      
         ),
         
         ...history.map((place) => 
            ListTile(
              title: Text(place.text),
              subtitle: Text(place.placeName),
              leading: const Icon(Icons.history, color: Colors.black),  
              onTap: () {
                close(context, SearchResult(cancel: false, manual: false, place: place));
              },
            )
         ),
       ],
     );
  }

}