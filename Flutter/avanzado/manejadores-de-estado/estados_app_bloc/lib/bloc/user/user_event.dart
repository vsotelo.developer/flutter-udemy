
part of 'user_bloc.dart';

@immutable
abstract class UserEvent {}

class ActivateUser extends UserEvent {
  final Usuario newUser;
  ActivateUser(this.newUser);
}

class ChangeUserAge extends UserEvent {
  final int age;
  ChangeUserAge(this.age);
}

class AddProfesionUser extends UserEvent {
  final String prefesion;
  AddProfesionUser(this.prefesion);
}

class DeleteUser extends UserEvent {}