import 'package:bloc/bloc.dart';
import 'package:estados_app/models/usuario.dart';
import 'package:meta/meta.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(const UserInitalState()) {
    on<ActivateUser>((event, emit) {      
      emit(UserSetState(event.newUser));
    });

    on<DeleteUser>((event, emit) => emit(const UserInitalState()));

    on<ChangeUserAge>((event, emit) {      
      if(!state.existUser) return;
      emit(UserSetState(state.usuario!.copyWith(edad: event.age)));
    });

    on<AddProfesionUser>((event, emit) {
      if(!state.existUser) return;
        //List<String> profesiones = state.usuario!.profesiones;
        //profesiones.add(event.prefesion);
        final profesions = [...state.usuario!.profesiones, event.prefesion];
        emit(UserSetState(state.usuario!.copyWith(profesiones: profesions)));
    });
  }
}