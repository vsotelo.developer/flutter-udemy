import 'package:estados_app/bloc/user/user_bloc.dart';
import 'package:estados_app/models/usuario.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Page2Screen extends StatelessWidget {
  const Page2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final UserBloc userBlock =  BlocProvider.of<UserBloc>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagina 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(              
              child: const Text('Establecer Usuario', style: TextStyle(color: Colors.white)),              
              color: Colors.blue,            
              onPressed: () {
                userBlock.add(ActivateUser(Usuario(nombre: 'Victor Andres', edad: 28, profesiones: ['Ing Sistemas'])));

              },
            ),            
            MaterialButton(              
              child: const Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
              color: Colors.blue,            
              onPressed: () {
                  userBlock.add(ChangeUserAge(21));
              },
            ),
            MaterialButton(              
              child: const Text('Añadir Profesion', style: TextStyle(color: Colors.white)),
              color: Colors.blue,            
              onPressed: () {
                userBlock.add(AddProfesionUser('Contador'));
              },
            )            
          ],
        ),
      ),
    );
  }
}
