
import 'dart:async';

import 'package:estados_app/models/usuario.dart';

class _UsuarioService {  

  Usuario? _usuario;

  final StreamController<Usuario> _usuarioStreamController = StreamController<Usuario>.broadcast(); // agregar broadcast para emitir a mas de 1

  Stream<Usuario> get usuarioStream => _usuarioStreamController.stream;

  Usuario? get usuario => _usuario;
  bool get existeUsuario => _usuario != null;

  void cargarUsuario(Usuario user){    
    _usuario = user;
    _usuarioStreamController.add(user);
  }

  void cambiarEdad(int edad){
    usuario?.edad = edad;
    _usuarioStreamController.add(usuario!);
  }

  dispose() {
    _usuarioStreamController.close();
  }

}

final usuarioService = _UsuarioService();
