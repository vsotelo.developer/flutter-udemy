import 'package:estados_app/models/usuario.dart';
import 'package:estados_app/services/usuario.service.dart';
import 'package:flutter/material.dart';

class Page2Screen extends StatelessWidget {
  const Page2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StreamBuilder(
          stream: usuarioService.usuarioStream,          
          builder: (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
            return Container(
              child: snapshot.hasData ? Text('Nombre: ${snapshot.data!.nombre}') : const Text('Pagina 1'),
            );
          },
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(              
              child: const Text('Establecer Usuario', style: TextStyle(color: Colors.white)),
              color: Colors.blue,            
              onPressed: () {
                usuarioService.cargarUsuario(Usuario(nombre: 'Victor', edad: 27, profesiones: ["Ing. Sistemas", "Fisico Nuclear"]));
              },
            ),            
            MaterialButton(              
              child: const Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
              color: Colors.blue,            
              onPressed: () {
                usuarioService.cambiarEdad(30);
              },
            ),
            MaterialButton(              
              child: const Text('Añadir Profesion', style: TextStyle(color: Colors.white)),              
              color: Colors.blue,
              onPressed: () {},
            )            
          ],
        ),
      ),
    );
  }
}
