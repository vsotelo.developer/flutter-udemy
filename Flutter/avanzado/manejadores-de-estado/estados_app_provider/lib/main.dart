import 'package:estados_app/pages/pagina1.dart';
import 'package:estados_app/pages/pagina2.dart';
import 'package:estados_app/service/usuario.service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UsuarioService())
      ],
      child: MaterialApp(
        title: 'Material App',
        debugShowCheckedModeBanner: false,
        initialRoute: 'pagina1',
        routes: {
           'pagina1': (context) => const Page1Screen(),
           'pagina2': (context) => const Page2Screen(),
        },
      ),
    );
  }
}