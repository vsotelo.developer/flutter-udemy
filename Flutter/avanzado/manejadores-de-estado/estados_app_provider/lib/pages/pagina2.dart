import 'package:estados_app/models/usuario.dart';
import 'package:estados_app/service/usuario.service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Page2Screen extends StatelessWidget {
  const Page2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final usuarioService = Provider.of<UsuarioService>(context);

    return Scaffold(
      appBar: AppBar(
        title:usuarioService.existeUsuario ? Text('Nombre: ' + usuarioService.usuario.nombre) : const Text('Pagina 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(              
              child: const Text('Establecer Usuario', style: TextStyle(color: Colors.white)),
              color: Colors.blue,            
              onPressed: () {
                final usuarioService = Provider.of<UsuarioService>(context, listen: false);
                usuarioService.usuario = Usuario(nombre: 'Victor', edad: 17, profesiones: ['Ing Sistemas', 'Fisica Nuclear']);
              },
            ),            
            MaterialButton(
              onPressed: () {
                final usuarioService = Provider.of<UsuarioService>(context, listen: false);
                usuarioService.cambiarEdad(28);
              },
              child: const Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
              color: Colors.blue,            
            ),
            MaterialButton(
              onPressed: () {
                 final usuarioService = Provider.of<UsuarioService>(context, listen: false);
                 usuarioService.agregarProfesion();
              },
              child: const Text('Añadir Profesion', style: TextStyle(color: Colors.white)),
              color: Colors.blue,            
            )            
          ],
        ),
      ),
    );
  }
}
