import 'package:estados_app/models/usuario.dart';
import 'package:estados_app/service/usuario.service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Page1Screen extends StatelessWidget {
  const Page1Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final usuarioService = Provider.of<UsuarioService>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagina 1'),
        actions: [
          IconButton(            
            icon: const Icon(Icons.exit_to_app),
            onPressed: () => usuarioService.removerUsuario(),
          )                      
        ],
      ),
      body: usuarioService.existeUsuario ? InformacionUsuario(usuario: usuarioService.usuario) : const Center(child: Text('No hay usuario seleccionado.'),),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, 'pagina2'),
        child: const Icon(Icons.accessibility_new),
      ),
    );
  }
}

class InformacionUsuario extends StatelessWidget {
  const InformacionUsuario({Key? key, required this.usuario}) : super(key: key);

  final Usuario usuario;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:  [
          const Text('General', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          const Divider(),

          ListTile(title: Text('Nombre: ' + usuario.nombre)),
          ListTile(title: Text('Edad: ' + usuario.edad.toString())),

          const Text('Profesiones', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          const Divider(),

          //const ListTile(title: Text('Profesion 1: ')),

          ...usuario.profesiones.map(
            (profesion) => ListTile(title: Text(profesion))
          ).toList(),

        ],
      ),
    );
  }
}
