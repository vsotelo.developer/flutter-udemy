import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:stripe_app/bloc/pagar/pagar_bloc.dart';
import 'package:stripe_app/data/tarjetas.dart';
import 'package:stripe_app/helpers/alertas.dart';
import 'package:stripe_app/helpers/navegar_fadein.dart';
import 'package:stripe_app/pages/tarjeta_page.dart';
import 'package:stripe_app/services/stripe_service.dart';
import 'package:stripe_app/widgets/total_page_botton.dart';

class HomPage extends StatelessWidget {
  HomPage({Key? key}) : super(key: key);

  final stripeService = StripeService();

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;
    final pagarBloc = BlocProvider.of<PagarBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Pagar')),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () async {
              //mostrarAlerta(context, 'Hola', 'Mundo');
              //await Future.delayed(const Duration(seconds: 1));
              //Navigator.pop(context);
              final res = await stripeService.pagarConNuevaTarjeta(amount: pagarBloc.state.montoPagarString, currency: pagarBloc.state.moneda);

              if(res.ok) {
                mostrarAlerta(context, 'Tarjeta OK', 'Todo Correcto');
              } else {
                mostrarAlerta(context, 'Algo salio mal', res.msg!);  
              }
            },
          )
        ],
      ),
      body: Stack(
        children: [
          Positioned(            
            width: size.width,
            height: size.height,
            top: 200,
            child: PageView.builder(
              controller: PageController(
                viewportFraction: 0.9
              ),
              physics: const BouncingScrollPhysics(),
              itemCount: tarjetas.length,
              itemBuilder: (context, index) {
                final tarjeta = tarjetas[index];
                return GestureDetector(
                  onTap: () {
                    pagarBloc.add(OnSeleccionarTarjeta(tarjeta));
                    Navigator.push(context, navegarFadeIn(context, const TarjetaPage()));
                  },
                  child: Hero(
                    tag: tarjeta.cardNumber,
                    child: CreditCardWidget(
                      cardNumber: tarjeta.cardNumber,
                      expiryDate: tarjeta.expiracyDate,
                      cardHolderName: tarjeta.cardHolderName,
                      cvvCode: tarjeta.cvv,
                      showBackView: false,
                      onCreditCardWidgetChange: (creditCardBrand) {},
                    ),
                  ),
                );
              },
            ),
          ),
          const Positioned(
            bottom: 0,
            child: TotalPageBotton()
          )
        ],
      ),
    );
  }
}
