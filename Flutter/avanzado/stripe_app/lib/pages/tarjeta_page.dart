import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';

import '../bloc/pagar/pagar_bloc.dart';
import '../widgets/total_page_botton.dart';

class TarjetaPage extends StatelessWidget {
   
  const TarjetaPage({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {

    final tarjeta = BlocProvider.of<PagarBloc>(context).state.tarjeta!;

    final pagarBloc = BlocProvider.of<PagarBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Pagar')),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            pagarBloc.add(OnDesactivarTarjeta());
            Navigator.pop(context);
          },
        ),
      ),      
      body: Stack(
        children: [
          Container(),
          Hero(
            tag: tarjeta.cardNumber,
            child: CreditCardWidget(
                cardNumber: tarjeta.cardNumber,
                expiryDate: tarjeta.expiracyDate,
                cardHolderName: tarjeta.cardHolderName,
                cvvCode: tarjeta.cvv,
                showBackView: false,
                onCreditCardWidgetChange: (creditCardBrand) {},
             ),
          ),          
           const Positioned(
            bottom: 0,
            child: TotalPageBotton()
          )          
        ]
      ),
    );
  }
}