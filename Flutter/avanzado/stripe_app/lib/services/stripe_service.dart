import 'package:dio/dio.dart';
import 'package:stripe_payment/stripe_payment.dart';

import '../model/payment_intent_response.dart';
import '../model/stripe_custom_response.dart';

class StripeService {

  // Singleton
  StripeService._privateConstructor();
  static final StripeService _intance = StripeService._privateConstructor();
  factory StripeService() => _intance;

  final String _paymentApiUrl = 'https://api.stripe.com/v1/payment_intents';
  static String _secretKey = 'sk_test_51KijYnCBGYmTgjJ3CZQdLbWF4zo5HlAze10vTu8RhIqn0UTmAoekTzFHo0135RRd0kuzFUwQ9O4JcJZckhK4lP6p00vcoJq6jH';
  final String _apiKey    = 'pk_test_51KijYnCBGYmTgjJ3JSjeJB2Hnju4SBBc7SngVkcosg3sVAFAhLZZH8ENEikmXKs36f9MYru60qz93UxJLHjrjV3y00PXQFbuIB';

  final headerOptions = Options(
    contentType: Headers.formUrlEncodedContentType,
    headers: {
      'Authorization': 'Bearer ${ StripeService._secretKey }'
    }
  );

  void init() { 
    StripePayment.setOptions(StripeOptions(publishableKey: _apiKey, androidPayMode: 'test', merchantId: 'test'));
   }

  Future pagarConTarjetaExiste({ required String amount, required String currency,  required CreditCard card }) async {

  }

  Future<StripeCustomResponse> pagarConNuevaTarjeta({ required String amount, required String currency }) async {
   
   try {

      final paymentMethod = await StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest());
      //final resp = await _realizarPago(amount: amount, currency: currency, paymentMethod: paymentMethod);

      return StripeCustomResponse(ok: true);
    } catch (e) {

      return StripeCustomResponse(ok: false, msg: e.toString());

    }
  }

  Future pagarApplePayGooglePay({ required String amount, required String currency }) async {
  }

  Future _crearPaymentIntent({ required String amount, required String currency }) async {

    try {
        final dio = Dio();
        final data = {'amount': amount, 'currency': currency};

        final resp =
            await dio.post(_paymentApiUrl, data: data, options: headerOptions);

        return PaymentIntentResponse.fromJson(resp.data);
      } catch (e) {
        print('Error en intento: ${e.toString()}');
        return null;
      }    

  }

  Future _realizarPago({ required String amount, required String currency, required PaymentMethod paymentMethod }) async {
  }

}
